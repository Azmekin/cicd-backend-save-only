import $api from "../http"

export default class AuthServise{
    static async login(username, password){
        return $api.post("/singin", {username, password})
    }

    static async reg(username, password){
        return $api.post("/singin", {username, password})
    }

    static async logout(){
        return $api.post("/logout")
    }
}