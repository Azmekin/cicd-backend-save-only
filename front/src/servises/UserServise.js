import $api from "../http"

export default class UserServise{
    static fetchUsers(){
        return $api.get('/users')
    }
}